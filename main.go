package main

import (
	"fmt"
	"service/presenter"
	"service/producer"
	"service/service"
)

func main() {
	prod := producer.NewFileProducer("input.txt")
	pres := presenter.NewFileWriterPresenter("output.txt")

	serv := service.NewService(prod, pres)

	err := serv.Run()
	if err != nil {
		fmt.Printf("Ошибка выполнения сервиса: %v\n", err)
		return
	}

	fmt.Println("Операция успешно выполнена")
}
