package service

import (
	"service/masklink"
	"sync"
)

type Producer interface {
	Produce() ([]string, error)
}

type Presenter interface {
	Present([]string) error
}

type Service struct {
	Prod Producer
	Pres Presenter
}

func NewService(prod Producer, pres Presenter) *Service {
	return &Service{
		Prod: prod,
		Pres: pres,
	}
}

func (s *Service) Run() error {
	data, err := s.Prod.Produce()
	if err != nil {
		return err
	}

	input := make(chan string)
	output := make(chan string)
	var wg sync.WaitGroup

	bufferChanel := make(chan struct{}, 10)

	go func() {
		for _, line := range data {
			input <- line
		}
		close(input)
	}()

	go func() {
		for text := range input {
			bufferChanel <- struct{}{} // блокируем, если количество горутин >= 10
			wg.Add(1)
			go func(text string) {
				defer wg.Done()
				result := masklink.MaskLink(text)
				output <- result
				<-bufferChanel // освобождаем место для новой горутины
			}(text)
		}
		wg.Wait()
		close(output)
	}()

	var maskedData []string
	for result := range output {
		maskedData = append(maskedData, result)
	}

	err = s.Pres.Present(maskedData)
	if err != nil {
		return err
	}

	return nil
}
