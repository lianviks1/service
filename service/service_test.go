package service

import (
	"errors"
	"io/ioutil"
	"service/masklink"
	"service/presenter"
	"service/producer"

	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockProducer struct {
	mock.Mock
}

func (m *MockProducer) Produce() ([]string, error) {
	args := m.Called()
	return args.Get(0).([]string), args.Error(1)
}

type MockPresenter struct {
	mock.Mock
}

func (m *MockPresenter) Present(data []string) error {
	args := m.Called(data)
	return args.Error(0)
}

func TestService_Run(t *testing.T) {
	mockProducer := new(MockProducer)
	mockPresenter := new(MockPresenter)

	producedData := []string{"http://example.com", "Check out this link: http://example.org"}
	mockProducer.On("Produce").Return(producedData, nil)
	mockPresenter.On("Present", mock.AnythingOfType("[]string")).Return(nil)

	service := Service{
		Prod: mockProducer,
		Pres: mockPresenter,
	}

	err := service.Run()

	assert.NoError(t, err)

	mockProducer.AssertCalled(t, "Produce")
	mockPresenter.AssertCalled(t, "Present", mock.AnythingOfType("[]string"))

	expectedMaskedData := []string{"http://***********", "Check out this link: http://***********"}

	args := mockPresenter.Calls[0].Arguments
	actualData := args.Get(0).([]string)

	for i, text := range actualData {
		actualData[i] = masklink.MaskLink(text)
	}

	assert.ElementsMatch(t, expectedMaskedData, actualData)
}

func TestService_ProducerError(t *testing.T) {
	mockPresenter := new(MockPresenter)
	filePath := "nonexistent_file.txt"
	fp := &producer.FileProducer{filePath}
	service := Service{
		Prod: fp,
		Pres: mockPresenter,
	}
	err := service.Run()
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "no such file or directory")
	mockPresenter.AssertNotCalled(t, "Present")
}

func TestService_PresentError(t *testing.T) {
	mockProducer := new(MockProducer)
	mockPresenter := new(MockPresenter)
	producedData := []string{"http://example.com", "Check out this link: http://example.org"}
	mockProducer.On("Produce").Return(producedData, nil)
	mockPresenter.On("Present", mock.AnythingOfType("[]string")).Return(errors.New("presentation error"))
	service := Service{
		Prod: mockProducer,
		Pres: mockPresenter,
	}
	err := service.Run()
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "presentation error")
	mockProducer.AssertCalled(t, "Produce")
	mockPresenter.AssertCalled(t, "Present", mock.AnythingOfType("[]string"))
}

func TestFileWriterPresenter_Present(t *testing.T) {
	data := []string{"http://example.com", "Check out this link: http://example.org"}
	filePath := "output.txt"
	presenter := presenter.FileWriterPresenter{filePath}
	var err = presenter.Present(data)
	assert.NoError(t, err)
	content, readErr := ioutil.ReadFile(filePath)
	assert.NoError(t, readErr)
	assert.Equal(t, "http://***********\nCheck out this link: http://***********\n", string(content))
}
