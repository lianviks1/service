package presenter

import (
	"fmt"
	"os"
	"strings"
)

type FileWriterPresenter struct {
	FilePath string
}

// NewFileWriterPresenter создает новый экземпляр FileWriterPresenter с указанным путем к файлу.
func NewFileWriterPresenter(filePath string) *FileWriterPresenter {
	return &FileWriterPresenter{
		FilePath: filePath,
	}
}

func (fwp *FileWriterPresenter) Present(data []string) error {
	output := strings.Join(data, "\n")
	err := os.WriteFile(fwp.FilePath, []byte(output), 0777)
	if err != nil {
		return fmt.Errorf("present() WriteFile(): %w", err)
	}
	return nil
}
