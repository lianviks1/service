package producer

import (
	"io/ioutil"
)

type FileProducer struct {
	FilePath string
}

func NewFileProducer(filePath string) *FileProducer {
	return &FileProducer{
		FilePath: filePath,
	}
}

func (fp *FileProducer) Produce() ([]string, error) {
	content, err := ioutil.ReadFile(fp.FilePath)
	if err != nil {
		return nil, err
	}
	return []string{string(content)}, nil
}
